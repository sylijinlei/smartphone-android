/*
 * smartphonecore.h
 *
 *  Created on: 2017年1月6日
 *      Author: lijinlei
 */

#ifndef SMARTPHONECORE_H_
#define SMARTPHONECORE_H_
#ifdef __cplusplus
extern "C" {
#endif


#define MAX_LENGTH 100
#define MAX_INFO 256

typedef enum {
    SMARTPHONE_DEBUG=1,
    SMARTPHONE_TRACE=1<<1,
    SMARTPHONE_MESSAGE=1<<2,
    SMARTPHONE_WARNING=1<<3,
    SMARTPHONE_ERROR=1<<4,
    SMARTPHONE_FATAL=1<<5,
    SMARTPHONE_LOGLEV_END=1<<6
} SmartphoneLogLevel;
typedef void (*SmartphoneLogFunc)(SmartphoneLogLevel lev, const char *fmt, va_list args);

typedef enum _SmartphoneState{
    /************** RegistrationState *******************/
    SmartphoneRegistrationNone,          /**<Initial state for registrations */
    SmartphoneRegistrationProgress,     /**<Registration is in progress */
    SmartphoneRegistrationOk,           /**<Registration is successful */
    SmartphoneRegistrationCleared,       /**<Unregistration succeeded */
    SmartphoneRegistrationFailed,	/**<Registration failed */

    /************* CallState **************************/
    SmartphoneCallIdle,					/**<Initial call state */
    SmartphoneCallIncomingReceived, /**<This is a new incoming call */
    SmartphoneCallOutgoingInit, /**<An outgoing call is started */
    SmartphoneCallOutgoingProgress, /**<An outgoing call is in progress */
    SmartphoneCallOutgoingRinging, /**<An outgoing call is ringing at remote end */
    SmartphoneCallOutgoingEarlyMedia, /**<An outgoing call is proposed early media */
    SmartphoneCallConnected, /**<Connected, the call is answered */
    SmartphoneCallStreamsRunning, /**<The media streams are established and running*/
    SmartphoneCallPausing, /**<The call is pausing at the initiative of local end */
    SmartphoneCallPaused, /**< The call is paused, remote end has accepted the pause */
    SmartphoneCallResuming, /**<The call is being resumed by local end*/
    SmartphoneCallRefered, /**<The call is being transfered to another party, resulting in a new outgoing call to follow immediately*/
    SmartphoneCallError, /**<The call encountered an error*/
    SmartphoneCallEnd, /**<The call ended normally*/
    SmartphoneCallPausedByRemote, /**<The call is paused by remote end*/
    SmartphoneCallUpdatedByRemote, /**<The call's parameters change is requested by remote end, used for example when video is added by remote */
    SmartphoneCallIncomingEarlyMedia, /**<We are proposing early media to an incoming call */
    SmartphoneCallUpdating, /**<A call update has been initiated by us */
    SmartphoneCallReleased, /**< The call object is no more retained by the core */
    SmartphoneCallEarlyUpdatedByRemote, /*<The call is updated by remote while not yet answered (early dialog SIP UPDATE received).*/
    SmartphoneCallEarlyUpdating,/*<We are updating the call while not yet answered (early dialog SIP UPDATE sent)*/

    /************* CallInfo **************************/
    SmartphoneCallInfoReceived

}SmartphoneState;
typedef struct _RegistrationStateInfo{

    char proxy[MAX_LENGTH];
    const char *message;
}RegistrationStateInfo;
typedef struct _CallStateInfo{
    long callid;
    char dispname[MAX_LENGTH];
    char username[MAX_LENGTH];
    const char *message;
}CallStateInfo;
typedef struct _CallInfo{
    long callid;
    char info[MAX_INFO];
}CallInfo;
typedef struct _SmartphoneCallbackParam{
    SmartphoneState state;
    void *data;
} SmartphoneCallbackParam;
typedef void (*SmartphoneCallback)(SmartphoneCallbackParam *param);

typedef enum{
    SmartphoneTransportTypeUdp = 0,
    SmartphoneTransportTypeTcp,
    SmartphoneTransportTypeTls,
    SmartphoneTransportTypeDtls
}SmartphoneTransportType;

typedef enum{
    MediaTypeAudio = 0,
    MediaTypeVideo,
    MediaTypeAudioVideo
}MediaType;

typedef enum _MediaDirection {
        MediaDirectionInvalid = -1,
        MediaDirectionInactive, /** No active media not supported yet*/
        MediaDirectionSendOnly, /** Send only mode*/
        MediaDirectionRecvOnly, /** recv only mode*/
        MediaDirectionSendRecv, /** send receive*/
} MediaDirection;
typedef enum{
    CodecTypeAudio = 0,
    CodecTypeVideo,
    CodecTypeText
}CodecType;

void smartphone_iterate();
int smartphone_init(SmartphoneCallback callback, SmartphoneLogFunc func);
void smartphone_uninit();

int smartphone_register(const char *server_addr, SmartphoneTransportType portType,
                        const char *dispname, const char *username, const char* passwd, int expires);
void smartphone_unregister();
void smartphone_refresh_registers();

void smartphone_call(const char *identity, MediaType mt);
void smartphone_answer(long callid, MediaType mt);
void smartphone_terminate(long callid);
int  smartphone_call_video_enabled(long callid);
int smartphone_get_calls_nb();

void smartphone_set_native_window_id(long id);
long smartphone_get_native_window_id();
void smartphone_set_native_preview_window_id(long id);
long smartphone_get_native_preview_window_id();



void smartphone_send_info(const char *message, long callid);

void smartphone_set_static_picture(const char *picture);


int smartphone_get_codec_size(CodecType ct);
const char *smartphone_get_codec_name(CodecType ct, int id);
int smartphone_get_codec_clock_rate(CodecType ct, int id);
void smartphone_enable_codec(CodecType ct, int id, int enabled);

int smartphone_get_video_device_size();
const char *smartphone_get_video_device_name(int id);
void smartphone_set_video_device(const char *name);

int smartphone_get_supported_video_size();
const char *smartphone_get_supported_video_size_name(int id);
void smartphone_set_preferred_video_size(const char *name);

void smartphone_set_primary_contact(const char *dispname, const char *username);

void smartphone_enable_echo_cancellation(int enabled);
void smartphone_enable_adaptive_rate_control(int enabled);
void smartphone_set_local_port(int port);
void smartphone_set_record_file(const char *path);
void smartphone_start_recording();
void smartphone_stop_recording();
void smartphone_take_preview_snapshot(long callid, const char *path);


void smartphone_set_ring(const char *ring);                           //设置被呼叫者的铃声
void smartphone_set_ringback(char *ringback);                   //设置呼叫者呼叫时的提示音，一般为嘟嘟声
void smartphone_set_remote_ringback(char *remote_ringback);     //设置呼叫者回铃音， 类似彩铃

void smartphone_set_static_picture(const char *picture);



void smartphone_reload_ms_plugins(const char *path);



long smartphone_create_local_player(long windowid);
void smartphone_player_destroy(long player);
void smartphone_player_open(long player, const char *filename);
void smartphone_player_close(long player);
void smartphone_player_start(long player);
void smartphone_player_pause(long player);
#ifdef __cplusplus
}
#endif

#endif /* SMARTPHONECORE_H_*/
