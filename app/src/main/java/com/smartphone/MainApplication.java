package com.smartphone;

import android.app.Application;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.CheckBoxPreference;
import android.support.v7.app.NotificationCompat;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.smartphone.core.CodecType;
import com.smartphone.core.MediaType;
import com.smartphone.core.Smartphone;
import com.smartphone.core.SmartphoneState;
import com.smartphone.service.SmartphoneService;
import com.smartphone.ui.CallActivity;
import com.smartphone.ui.CallHandleActivity;

import org.linphone.mediastream.MediastreamerAndroidContext;
import org.w3c.dom.Text;

/**
 * Created by lijinlei on 17-9-15.
 */

public class MainApplication extends Application {
    private static final String TAG = "MainApplication";
    private Smartphone sp;
    private String myprefs = "myprefs";
    private String settings = "settings";
    private CallReceiver mCallReceiver = null;
    private int CallState = SmartphoneState.CallIdle;

    public SharedPreferences getMyprefs() {
        return this.getSharedPreferences(myprefs, MODE_PRIVATE);
    }

    public SharedPreferences getSettingsPrefs() {
        return this.getSharedPreferences(settings, MODE_PRIVATE);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        SharedPreferences preferences = getMyprefs();

        sp = Smartphone.getInstance();
        MediastreamerAndroidContext.setContext(this);
        sp.init();
       // initAllCodecs();
        initVideoDevice();
        initVideoSizes();
        sp.setContext(this);
        sp.setPowerManager(getSystemService(Context.POWER_SERVICE));
        IntentFilter filter = new IntentFilter();
        filter.addAction(Smartphone.BroadcastCall);
        mCallReceiver = new CallReceiver();
        this.registerReceiver(mCallReceiver, filter);

        Intent intent = new Intent(this, SmartphoneService.class);
        startService(intent);
        AudioManager manager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        manager.setSpeakerphoneOn(false);
        sp.setRecordFile("/sdcard/record.mkv");
        sp.reloadPlugins(getFilesDir().getAbsolutePath() + "/../lib");

    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }


    class CallReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            int state = bundle.getInt("state");
            switch (state) {
                case SmartphoneState.CallOutgoingRinging:
                case SmartphoneState.CallOutgoingProgress:
                case SmartphoneState.CallOutgoingInit:
                    intent = new Intent(MainApplication.this, CallHandleActivity.class);
                    intent.putExtras(bundle);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    break;
                case SmartphoneState.CallIncomingReceived:
                    if (getSettingsPrefs().getBoolean("auto_answer", false)) {
                        sp.answer(bundle.getLong("callid"), MediaType.AudioVideo);
                    } else {
                        intent = new Intent(MainApplication.this, CallHandleActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtras(bundle);
                        startActivity(intent);
                    }
                    break;
                case SmartphoneState.CallConnected:
                case SmartphoneState.CallStreamsRunning:
                    intent = new Intent(MainApplication.this, CallActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtras(bundle);
                    startActivity(intent);
                    break;
                case SmartphoneState.CallError:
                    String message = bundle.getString("message");
                    Toast.makeText(MainApplication.this, message, Toast.LENGTH_LONG).show();
                    break;

            }

        }
    }

    void initAllCodecs() {
        int size = sp.getCodecSize(CodecType.Audio);
        for (int i = 0; i < size; i++) {
            String name = sp.getCodecName(CodecType.Audio, i);
            String key = name + "_" + i;
            Boolean first_start = getMyprefs().getBoolean("first_start", true);
            if(first_start){
                Log.d(TAG, "true");
                sp.enableCodec(CodecType.Audio, i, true);
            }else {
                Log.d(TAG, "false");
                boolean enabled = getSettingsPrefs().getBoolean(key, false);
                sp.enableCodec(CodecType.Audio, i, enabled);
            }
        }
        size = sp.getCodecSize(CodecType.Video);
        for (int i = 0; i < size; i++) {
            String name = sp.getCodecName(CodecType.Video, i);
            String key = name + "_" + i;
            Boolean first_start = getMyprefs().getBoolean("first_start", true);
            if(first_start){
                sp.enableCodec(CodecType.Video, i, true);
            }else{
                boolean enabled = getSettingsPrefs().getBoolean(key, false);
                sp.enableCodec(CodecType.Video, i, enabled);
            }

        }
    }

    void initVideoDevice() {
        String name = getSettingsPrefs().getString("video_devices", null);
        sp.setVideoDevice(name);
    }
    void initVideoSizes(){
        String name = getSettingsPrefs().getString("video_sizes", null);
        sp.setPreferredVideoSize(name);
    }
}
