package com.smartphone.core;

/**
 * Created by lijinlei on 17-9-8.
 */

public interface MediaType {
    public static final int Audio = 0;
    public static final int Video = 1;
    public static final int AudioVideo = 2;
}
