package com.smartphone.core;

/**
 * Created by lijinlei on 17-9-14.
 */
public interface    SmartphoneState {
        public static final int RegistrationNone = 0;          /**<Initial state for registrations */
        public static final int RegistrationProgress = 1;     /**<RegistrationReceiver is in progress */
        public static final int RegistrationOk = 2;           /**<RegistrationReceiver is successful */
        public static final int RegistrationCleared = 3;       /**<Unregistration succeeded */
        public static final int RegistrationFailed = 4;	/**<RegistrationReceiver failed */

        /************* CallState **************************/
        public static final int CallIdle = 5;					/**<Initial call state */
        public static final int CallIncomingReceived = 6; /**<This is a new incoming call */
        public static final int CallOutgoingInit = 7; /**<An outgoing call is started */
        public static final int CallOutgoingProgress = 8; /**<An outgoing call is in progress */
        public static final int CallOutgoingRinging = 9; /**<An outgoing call is ringing at remote end */
        public static final int CallOutgoingEarlyMedia = 10; /**<An outgoing call is proposed early media */
        public static final int CallConnected = 11; /**<Connected, the call is answered */
        public static final int CallStreamsRunning = 12; /**<The media streams are established and running*/
        public static final int CallPausing = 13; /**<The call is pausing at the initiative of local end */
        public static final int CallPaused = 14; /**< The call is paused, remote end has accepted the pause */
        public static final int CallResuming = 15; /**<The call is being resumed by local end*/
        public static final int CallRefered = 16; /**<The call is being transfered to another party, resulting in a new outgoing call to follow immediately*/
        public static final int CallError = 17; /**<The call encountered an error*/
        public static final int CallEnd = 18; /**<The call ended normally*/
        public static final int CallPausedByRemote = 19; /**<The call is paused by remote end*/
        public static final int CallUpdatedByRemote = 20; /**<The call's parameters change is requested by remote end, used for example when video is added by remote */
        public static final int CallIncomingEarlyMedia = 21; /**<We are proposing early media to an incoming call */
        public static final int CallUpdating = 22; /**<A call update has been initiated by us */
        public static final int CallReleased = 23; /**< The call object is no more retained by the core */
        public static final int CallEarlyUpdatedByRemote = 24; /*<The call is updated by remote while not yet answered (early dialog SIP UPDATE received).*/
        public static final int CallEarlyUpdating =25;/*<We are updating the call while not yet answered (early dialog SIP UPDATE sent)*/

}
