package com.smartphone.core;

/**
 * Created by lijinlei on 17-9-8.
 */
public interface SmartphoneTransportType {
    public static final int Udp = 0;
    public static final int Tcp = 1;
    public static final int Tls = 2;
    public static final int Dtls = 3;
}
