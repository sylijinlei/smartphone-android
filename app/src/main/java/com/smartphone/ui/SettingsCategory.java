package com.smartphone.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.preference.PreferenceCategory;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.smartphone.R;

/**
 * Created by lijinlei on 17-9-22.
 */

public class SettingsCategory extends PreferenceCategory {
    private String mTitle;
    private int mColor;
    private float mWidth;

    public SettingsCategory(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CategoryStyle);
        mColor = a.getInt(R.styleable.CategoryStyle_divider_color,
                Color.GRAY);
        mTitle = a.getString(R.styleable.CategoryStyle_title);
        mWidth = a.getDimension(R.styleable.CategoryStyle_divider_width, 10);


    }

    protected View onCreateView(ViewGroup parent) {
        super.onCreateView(parent);
        return LayoutInflater.from(getContext()).inflate(R.layout.settings_category, parent, false);
    }

    @Override
    protected void onBindView(View view) {
        super.onBindView(view);

        //在这里设置颜色值和字体大小
        TextView tv = (TextView) view.findViewById(R.id.preference_title);
        tv.setTextSize(16);
        tv.setTextColor(Color.BLACK);
        tv.setText(mTitle);

        //设置线的颜色
        View divider = (View) view.findViewById(R.id.preference_divider);
        LinearLayout.LayoutParams linearParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, (int)mWidth);
        divider.setLayoutParams(linearParams);
        divider.setBackgroundColor(mColor);

    }
}