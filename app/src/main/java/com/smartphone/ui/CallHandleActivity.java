package com.smartphone.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.camera2.CameraManager;
import android.media.Image;
import android.os.Handler;
import android.os.Message;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.telecom.Call;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.smartphone.MainApplication;
import com.smartphone.R;
import com.smartphone.core.MediaType;
import com.smartphone.core.Smartphone;
import com.smartphone.core.SmartphoneState;

import org.w3c.dom.Text;

public class CallHandleActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "CallHandleActivity";
    private Smartphone sp = null;
    private TextView tvDispname = null;
    private TextView tvUsername = null;
    private LinearLayout llBottom = null;
    private CallReceiver mCallReceiver = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
                WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON |
                WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_call_handle);

        sp = Smartphone.getInstance();
        if (sp.getCallState() == SmartphoneState.CallEnd || sp.getCallState() == SmartphoneState.CallError
            || sp.getCallState() == SmartphoneState.CallReleased){
        this.finish();
        }
        IntentFilter filter = new IntentFilter();
        filter.addAction(Smartphone.BroadcastCall);
        mCallReceiver = new CallReceiver();

        Log.d(TAG, "REGISTER RECEIVER");
        this.registerReceiver(mCallReceiver, filter);
        TextView tvTitle = (TextView) findViewById(R.id.tvTitle);
        tvDispname = (TextView) findViewById(R.id.tvDispname);
        tvUsername = (TextView) findViewById(R.id.tvUsername);
        llBottom = (LinearLayout) findViewById(R.id.llBottom);
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        int state = bundle.getInt("state", 0);
        String dispname = bundle.getString("dispname", "");
        String username = bundle.getString("username", "");
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT, 1);

        if (state == SmartphoneState.CallOutgoingInit) {
            tvTitle.setText(getResources().getString(R.string.outgoing));
            LayoutInflater inflater = getLayoutInflater();
            View view = inflater.inflate(R.layout.outgoing_bottom, null);
            view.setLayoutParams(params);
            view.findViewById(R.id.btnHungup).setOnClickListener(this);
            llBottom.addView(view);
        } else if (state == SmartphoneState.CallIncomingReceived) {
            tvTitle.setText(getResources().getString(R.string.incoming));
            LayoutInflater inflater = getLayoutInflater();
            View view = inflater.inflate(R.layout.incoming_bottom, null);
            view.setLayoutParams(params);
            view.findViewById(R.id.btnHungup).setOnClickListener(this);
            view.findViewById(R.id.btnAudio).setOnClickListener(this);
            view.findViewById(R.id.btnVideo).setOnClickListener(this);
            llBottom.addView(view);
        }
        tvDispname.setText(dispname);
        tvUsername.setText(username);

    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy--->");
        unregisterReceiver(mCallReceiver);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        sp.terminate(sp.getCurrentCallid());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnHungup:
                sp.terminate(sp.getCurrentCallid());
                this.finish();
                break;
            case R.id.btnAudio:
                sp.answer(sp.getCurrentCallid(), MediaType.Audio);
                break;
            case R.id.btnVideo:
                sp.answer(sp.getCurrentCallid(), MediaType.AudioVideo);
                break;
        }
    }

    class CallReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            int state = bundle.getInt("state");
            long callid = bundle.getLong("callid");
            switch (state) {
                case SmartphoneState.CallConnected:
                     intent = new Intent(CallHandleActivity.this, CallActivity.class);
                    intent.putExtras(bundle);
                    startActivity(intent);

                    break;
                case SmartphoneState.CallEnd:
                case SmartphoneState.CallReleased:
                    if (callid == sp.getCurrentCallid()) {
                        CallHandleActivity.this.finish();
                    }
                    break;
            }
        }
    }
}
