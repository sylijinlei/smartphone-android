package com.smartphone.ui;

import android.content.SharedPreferences;
import android.os.Bundle;

import android.preference.CheckBoxPreference;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceCategory;
import android.preference.PreferenceFragment;
import android.util.Log;


import com.smartphone.MainApplication;
import com.smartphone.R;
import com.smartphone.core.CodecType;
import com.smartphone.core.Smartphone;

import java.util.List;

/**
 * Created by lijinlei on 17-9-14.
 */

public class SettingsFragment extends PreferenceFragment implements Preference.OnPreferenceChangeListener {
    private static final String TAG = "SettingsFragment";
    private CheckBoxPreference echo_cancellation;
    private CheckBoxPreference adaptive_rate;
    private EditTextPreference dispname;
    private EditTextPreference username;
    private EditTextPreference port;
    private PreferenceCategory pcAudioCodecs;
    private PreferenceCategory pcVideoCodecs;
    private ListPreference lpVideoDevices;
    private ListPreference lpVideoSizes;
    private Smartphone sp = Smartphone.getInstance();
    private AudioCodecPreferenceChangeListener mAudioCodecListener;
    private VideoCodecPreferenceChangeListener mVideoCodecListener;
    private MainApplication mainApplication;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getPreferenceManager().setSharedPreferencesName("settings");
        addPreferencesFromResource(R.xml.preferences_settings);
        mainApplication = (MainApplication) getActivity().getApplication();
        echo_cancellation = (CheckBoxPreference) findPreference("echo_cancellation");
        echo_cancellation.setOnPreferenceChangeListener(this);
        adaptive_rate = (CheckBoxPreference) findPreference("adaptive_rate");
        adaptive_rate.setOnPreferenceChangeListener(this);

        dispname = (EditTextPreference) findPreference("dispname");
        dispname.setSummary(dispname.getText());
        dispname.setOnPreferenceChangeListener(this);
        username = (EditTextPreference) findPreference("username");
        username.setSummary(username.getText());
        username.setOnPreferenceChangeListener(this);

        port = (EditTextPreference) findPreference("port");
        port.setSummary(port.getText());
        port.setOnPreferenceChangeListener(this);

        pcAudioCodecs = (PreferenceCategory) findPreference("audio_codecs");
        pcVideoCodecs = (PreferenceCategory) findPreference("video_codecs");
        mAudioCodecListener = new AudioCodecPreferenceChangeListener();
        mVideoCodecListener = new VideoCodecPreferenceChangeListener();
        initAllCodecs();

        lpVideoDevices = (ListPreference) findPreference("video_devices");
        lpVideoDevices.setSummary(lpVideoDevices.getValue());
        lpVideoDevices.setOnPreferenceChangeListener(this);
        initVideoDevices();

        lpVideoSizes = (ListPreference) findPreference("video_sizes");
        lpVideoSizes.setSummary(lpVideoSizes.getValue());
        lpVideoSizes.setOnPreferenceChangeListener(this);
        initVideoSizes();

        SharedPreferences preferences = mainApplication.getMyprefs();
        Boolean first_start = preferences.getBoolean("first_start", true);
        if(first_start){
            SharedPreferences.Editor editor  = preferences.edit();
            editor.putBoolean("first_start", false);
            editor.commit();
        }
    }


    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
        switch (preference.getKey()) {
            case "echo_cancellation":
                sp.enableEchoCancellation((boolean)newValue);
                break;
            case "adaptive_rate":
                sp.enableAdaptiveRrateControl((boolean)newValue);
                break;
            case "port":
                String value = (String)newValue;
                port.setSummary(value);
                int newport = Integer.valueOf(value);
                sp.setLocalPort(newport);
                break;
            case "dispname":
                dispname.setSummary((String) newValue);
                sp.setPrimaryContact((String)newValue, null);
                break;
            case "username":
                username.setSummary((String) newValue);
                sp.setPrimaryContact(null, (String)newValue);
                break;
            case "video_devices":
                lpVideoDevices.setSummary((String) newValue);
                sp.setVideoDevice((String) newValue);
                break;
            case "video_sizes":
                lpVideoSizes.setSummary((String) newValue);
                sp.setPreferredVideoSize((String)newValue);
                break;

        }

        return true;
    }

    void initAllCodecs() {
        int size = sp.getCodecSize(CodecType.Audio);
        for (int i = 0; i < size; i++) {
            String name = sp.getCodecName(CodecType.Audio, i);
            int clock = sp.getCodecClockRate(CodecType.Audio, i);
            CheckBoxPreference cb = new CheckBoxPreference(getActivity());
            cb.setKey(name + "_" + i);
            cb.setTitle(name);
            cb.setSummary(String.valueOf(clock) + " Hz");
            cb.setOnPreferenceChangeListener(mAudioCodecListener);
            pcAudioCodecs.addPreference(cb);
            Boolean first_start = mainApplication.getMyprefs().getBoolean("first_start", true);
            if(first_start){
                cb.setChecked(true);
            }
            sp.enableCodec(CodecType.Audio, i, cb.isChecked());
        }
        size = sp.getCodecSize(CodecType.Video);
        for (int i = 0; i < size; i++) {
            String name = sp.getCodecName(CodecType.Video, i);
            int clock = sp.getCodecClockRate(CodecType.Video, i);
            CheckBoxPreference cb = new CheckBoxPreference(getActivity());
            cb.setKey(name + "_" + i);
            cb.setTitle(name);
            cb.setOnPreferenceChangeListener(mVideoCodecListener);
            pcVideoCodecs.addPreference(cb);
            Boolean first_start = mainApplication.getMyprefs().getBoolean("first_start", true);
            if(first_start){
                cb.setChecked(true);
            }
            sp.enableCodec(CodecType.Video, i, cb.isChecked());
        }

    }

    class AudioCodecPreferenceChangeListener implements Preference.OnPreferenceChangeListener {

        @Override
        public boolean onPreferenceChange(Preference preference, Object newValue) {
            CheckBoxPreference cb = (CheckBoxPreference) preference;
            String key = cb.getKey();
            int id = Integer.valueOf(key.split("_")[1]);

            sp.enableCodec(CodecType.Audio, id, (boolean) newValue);
            return true;
        }
    }

    class VideoCodecPreferenceChangeListener implements Preference.OnPreferenceChangeListener {

        @Override
        public boolean onPreferenceChange(Preference preference, Object newValue) {
            CheckBoxPreference cb = (CheckBoxPreference) preference;
            String key = cb.getKey();
            int id = Integer.valueOf(key.split("_")[1]);
            sp.enableCodec(CodecType.Video, id, (boolean) newValue);
            return true;
        }
    }

    public void initVideoDevices() {
        int size = sp.getVideoDeviceSize();
        String[] entries = new String[size];
        for (int i = 0; i < size; i++) {
            String name = sp.getVideoDeviceName(i);
            entries[i] = name;
        }
        lpVideoDevices.setEntries(entries);
        lpVideoDevices.setEntryValues(entries);
        sp.setVideoDevice(lpVideoDevices.getValue());
    }

    public void initVideoSizes() {
        int size = sp.getSupportedVideoSize();
        String[] entries = new String[size];
        for (int i = 0; i < size; i++) {
            String name = sp.getSupportedVideoSizeName(i);
            entries[i] = name;
        }
        lpVideoSizes.setEntries(entries);
        lpVideoSizes.setEntryValues(entries);
        sp.setPreferredVideoSize(lpVideoSizes.getValue());
    }
}
