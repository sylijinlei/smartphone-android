package com.smartphone.ui;

import android.app.Fragment;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.support.annotation.Nullable;
import android.support.v7.preference.PreferenceFragmentCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.smartphone.MainApplication;
import com.smartphone.R;
import com.smartphone.core.MediaType;
import com.smartphone.core.Smartphone;

/**
 * Created by lijinlei on 17-9-14.
 */

public class DialFragment extends Fragment {
    private static final String TAG = "DialFragment";
    private EditText edtIdentity = null;
    private Button btnCall = null;
    private RadioGroup radioMediaType = null;
    SharedPreferences shared = null;
    SharedPreferences.Editor editor = null;



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        MainApplication app = (MainApplication) getActivity().getApplication();
        shared = app.getMyprefs();
        editor = shared.edit();
        final View view = inflater.inflate(R.layout.fragment_dial, null);
        edtIdentity = (EditText) view.findViewById(R.id.edtIdentity);
        radioMediaType = (RadioGroup) view.findViewById(R.id.radioMediaType);
        int type = shared.getInt("radioMediaType", 0);
        ((RadioButton)radioMediaType.getChildAt(type)).setChecked(true);
        btnCall = (Button) view.findViewById(R.id.btnCall);

        btnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                RadioButton radio = (RadioButton) view.findViewById(radioMediaType.getCheckedRadioButtonId());
                int type = radioMediaType.indexOfChild(radio);

                String identity = edtIdentity.getText().toString();
                if (identity.isEmpty()) {
                    identity = shared.getString("edtIdentity", "");
                    edtIdentity.setText(identity);
                    edtIdentity.setSelection(edtIdentity.length());
                    return;
                }

                Smartphone.getInstance().call(identity, type);

                editor.putString("edtIdentity", identity);
                editor.putInt("radioMediaType", type);
                editor.commit();

            }
        });

        return view;
    }
}
