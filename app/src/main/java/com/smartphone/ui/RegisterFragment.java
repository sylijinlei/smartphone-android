package com.smartphone.ui;

import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.preference.PreferenceFragmentCompat;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.smartphone.MainApplication;
import com.smartphone.R;
import com.smartphone.core.CodecType;
import com.smartphone.core.Smartphone;
import com.smartphone.core.SmartphoneState;
import com.smartphone.service.SmartphoneService;


/**
 * Created by lijinlei on 17-9-14.
 */

public class RegisterFragment extends Fragment implements View.OnClickListener {
    private static final String TAG = "RegisterFragment";
    private Button btnRegister = null;
    private Button btnUnregister = null;
    private Button btnRefresh = null;
    private TextView tvRegisterStatus = null;
    private TextView tvMessage = null;
    private EditText edtUsername = null;
    private EditText edtPassword = null;
    private EditText edtServer = null;
    private CheckBox cbPassword = null;
    private RadioGroup radioTransport = null;
    private Smartphone sp = null;
    private SharedPreferences shared = null;
    private SharedPreferences.Editor editor = null;
    private RegistrationReceiver mRegistrationReceiver;

    private MainApplication app;

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void onResume() {
        super.onResume();
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_register, null);
        sp = Smartphone.getInstance();
        tvRegisterStatus = (TextView) view.findViewById(R.id.tvRegisterStatus);
        tvMessage = (TextView) view.findViewById(R.id.tvMessage);
        edtUsername = (EditText) view.findViewById(R.id.edtUsername);
        edtPassword = (EditText) view.findViewById(R.id.edtPassword);
        edtServer = (EditText) view.findViewById(R.id.edtServer);
        btnRegister = (Button) view.findViewById(R.id.btnRegister);
        btnRegister.setOnClickListener(this);
        btnUnregister = (Button) view.findViewById(R.id.btnUnregister);
        btnUnregister.setOnClickListener(this);
        btnRefresh = (Button) view.findViewById(R.id.btnRefresh);
        btnRefresh.setOnClickListener(this);
        radioTransport = (RadioGroup) view.findViewById(R.id.radioTransport);
        IntentFilter filter = new IntentFilter();
        filter.addAction(sp.BroadcastRegistration);
        mRegistrationReceiver = new RegistrationReceiver();
        getActivity().registerReceiver(mRegistrationReceiver, filter);
        sp.refreshRegisters();

        app = (MainApplication) getActivity().getApplication();
        shared = app.getMyprefs();
        editor = shared.edit();
        String username = shared.getString("edtUsername", null);
        edtUsername.setText(username);
        String password = shared.getString("edtPassword", null);
        edtPassword.setText(password);
        String server = shared.getString("edtServer", null);
        edtServer.setText(server);
        int type = shared.getInt("ratioTransport", 0);
        ((RadioButton) radioTransport.getChildAt(type)).setChecked(true);

        cbPassword = (CheckBox) view.findViewById(R.id.cbPassword);

        cbPassword.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    edtPassword.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                else
                    edtPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
            }
        });

        return view;
        }

        @Override
        public void onClick (View v){
            switch (v.getId()) {


                case R.id.btnRegister:
                    RadioButton radio = (RadioButton) getView().findViewById(radioTransport.getCheckedRadioButtonId());
                    int type = radioTransport.indexOfChild(radio);
                    String server = edtServer.getText().toString();
                    String dispname = app.getSettingsPrefs().getString("dispname", null);
                    if (server.isEmpty()) break;
                    sp.register(server, type,
                            dispname, edtUsername.getText().toString(), edtPassword.getText().toString(), 3600);
                    editor.putString("edtUsername", edtUsername.getText().toString());
                    editor.putString("edtPassword", edtPassword.getText().toString());
                    editor.putString("edtServer", edtServer.getText().toString());
                    editor.putInt("radioTransport", type);
                    editor.commit();

                    break;
                case R.id.btnUnregister:
                    sp.unregister();
                    break;
                case R.id.btnRefresh:
                    sp.refreshRegisters();
                    break;
            }
        }
        class RegistrationReceiver extends BroadcastReceiver {
            @Override
            public void onReceive(Context context, Intent intent) {
                Bundle bundle = intent.getExtras();
                int state = (int) bundle.get("state");
                String message = (String) bundle.get("message");
                AppCompatActivity activity = (AppCompatActivity) getActivity();
                ImageView imageView = (ImageView) activity.findViewById(R.id.ivRegister);
                switch (state) {
                    case SmartphoneState.RegistrationNone:
                        tvRegisterStatus.setText(getResources().getText(R.string.register_none));
                        tvRegisterStatus.setTextColor(ContextCompat.getColor(getActivity(), R.color.register_none));
                        tvMessage.setText("");
                        imageView.setImageDrawable(null);
                        break;
                    case SmartphoneState.RegistrationProgress:
                        tvRegisterStatus.setText(getResources().getText(R.string.register_progress));
                        tvRegisterStatus.setTextColor(ContextCompat.getColor(getActivity(), R.color.register_progress));
                        tvMessage.setText("");
                        tvMessage.setText("");
                        imageView.setImageDrawable(null);
                        break;
                    case SmartphoneState.RegistrationOk:
                        tvRegisterStatus.setText(getResources().getText(R.string.register_ok));

                        tvRegisterStatus.setTextColor(ContextCompat.getColor(getActivity(), R.color.register_ok));
                        tvMessage.setText("");
                        imageView.setImageDrawable(getResources().getDrawable(R.drawable.register));

                        break;
                    case SmartphoneState.RegistrationCleared:
                        tvRegisterStatus.setText(getResources().getText(R.string.register_cleared));
                        tvRegisterStatus.setTextColor(ContextCompat.getColor(getActivity(), R.color.register_cleared));
                        tvMessage.setText("");
                        imageView.setImageDrawable(null);
                        break;
                    case SmartphoneState.RegistrationFailed:
                        tvRegisterStatus.setText(getResources().getText(R.string.register_fail));
                        tvRegisterStatus.setTextColor(ContextCompat.getColor(getActivity(), R.color.register_fail));
                        tvMessage.setText(message);

                        imageView.setImageDrawable(null);
                        break;
                    default:
                        break;
                }
            }
        }
    }

