package com.smartphone.cmd;

import com.smartphone.core.Smartphone;

/**
 * Created by lijinlei on 17-9-20.
 */

public class CmdInfo {
    public void sendTapEvent(float x, float y) {
        String info = "input tap " + x + " "+ y;
        Smartphone.getInstance().sendInfo(info, Smartphone.getInstance().getCurrentCallid());
    }
}
