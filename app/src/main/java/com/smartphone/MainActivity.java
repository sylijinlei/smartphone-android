package com.smartphone;


import android.app.Fragment;
import android.app.FragmentManager;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentActivity;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.TextView;


import com.smartphone.ui.DialFragment;
import com.smartphone.ui.RegisterFragment;
import com.smartphone.ui.SettingsFragment;


import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    private ViewPager mViewPager = null;
    private MyFragmentPagerAdapter mAdapter = null;
    private List<Fragment> mListFragment = null;
    private TabLayout mTabLayout = null;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TextView tvTitle = (TextView) findViewById(R.id.tvTitle);
        tvTitle.setText(getResources().getString(R.string.tab_dial));
        mViewPager = (ViewPager) findViewById(R.id.viewpager);
        mListFragment = new ArrayList<>();
        mListFragment.add(new DialFragment());
        mListFragment.add(new RegisterFragment());
        mListFragment.add(new SettingsFragment());
        mAdapter = new MyFragmentPagerAdapter(getFragmentManager(), mListFragment);
        mViewPager.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
        mTabLayout = (TabLayout) findViewById(R.id.tablayout);
        mTabLayout.setupWithViewPager(mViewPager);
        mTabLayout.getTabAt(0).setCustomView(R.layout.tab_item_dial);
        mTabLayout.getTabAt(1).setCustomView(R.layout.tab_item_register);
        mTabLayout.getTabAt(2).setCustomView(R.layout.tab_item_settings);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                TextView tvTitle = (TextView) findViewById(R.id.tvTitle);
                switch (position) {
                    case 0:
                        tvTitle.setText(getResources().getString(R.string.tab_dial));
                        break;
                    case 1:
                        tvTitle.setText(getResources().getString(R.string.tab_register));
                        break;
                    case 2:
                        tvTitle.setText(getResources().getString(R.string.tab_settings));
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }


    private class MyFragmentPagerAdapter extends FragmentPagerAdapter {
        private List<Fragment> mFragments;


        public MyFragmentPagerAdapter(FragmentManager fm, List<Fragment> fragments) {
            super(fm);
            mFragments = fragments;
        }

        @Override
        public Fragment getItem(int position) {
            return mFragments.get(position);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }
    }

}
