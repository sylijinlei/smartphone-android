基于Linphone二次封装的简易通话软件，精简了复杂的linphone api，简单易移植。包含了二次封装的api源码和jni部分源码。

api说明：
核心类为Smartphone，单例模式。
使用步骤：

Smartphone sp = Smartphone.getInstance();

sp.init();  //初始化Smartphone

呼叫：sp.call(Strng identity); //identity为ip或用户名

登录：sp.register(...);

可通话注册和通话状态回调来获取通话状态，以便改变app的ui及相关逻辑。

注意：初次安装后未设置任何音视频格式， 呼叫会出现Not accept here， 这个提示说明媒体格式没有协商好，需要在设置里面设置音频或视频格式，两个客户端只要有一种媒体格式相同就可以通话啦！
